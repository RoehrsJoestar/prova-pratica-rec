using NUnit.Framework;


namespace prov.Tests
{
    public class Tests
    {

        [Test]
        public void CPF()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("33494263001"));
        }

        [Test]
        public void CNPJ()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("33173934000142"));
        }
    }

}